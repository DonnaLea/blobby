//
//  BlobbyView.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BlobbyView.h"
#import "BlobPoint.h"
#import "Blob.h"
#import "BezierPoint.h"
#import "Hull.h"

@implementation BlobbyView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
	if (blobbyViewController.displaySplash) {
		[blobbyViewController.splash drawInRect:[self bounds]];
	} else {
			
		// Drawing code
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		CGRect bounds = [self bounds];
		
		[[UIColor blackColor] set];
		CGContextFillRect(context, bounds);

		//draw the blobs
		for (Blob* blob in blobbyViewController.blobs) {
			[self drawBlob:blob];
		}
		
		/*
		[[UIColor redColor] set];
		CGContextBeginPath(context);
		CGContextMoveToPoint(context, 0, 410);
		CGContextAddLineToPoint(context, 320, 410);
		CGContextStrokePath(context);
		 */
	}
}

- (void) drawBlob:(Blob*) blob {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextAddPath(context, blob.path);
	
	[[UIColor greenColor] set];
//	[blob.colour set];


	CGContextFillPath(context);
//	CGContextDrawPath(context, kCGPathFill);
	
//	[self drawBlobHull:blob];
//	[self drawBezierPoints:blob];
	[self drawBlobPoints:blob];
}

- (void) drawBlobHull:(Blob*) blob {
	CGContextRef context = UIGraphicsGetCurrentContext();

	NSNumber* hullIndice = [blob.hull.hullIndices objectAtIndex:0];
	BlobPoint* firstBlobPoint = [blob.blobPoints objectAtIndex:[hullIndice intValue]]; 
	[[UIColor redColor] set];
	CGContextBeginPath(context);
	CGContextMoveToPoint(context, firstBlobPoint.point.x, firstBlobPoint.point.y);
	for (int i = 1; i < blob.hull.hullIndices.count; ++i) {
		hullIndice = [blob.hull.hullIndices objectAtIndex:i];
		BlobPoint* blobPoint = [blob.blobPoints objectAtIndex:[hullIndice intValue]];
		CGContextAddLineToPoint(context, blobPoint.point.x, blobPoint.point.y);
	}
	
	CGContextClosePath(context);
	CGContextStrokePath(context);
	
}

- (void) drawBlobPoints:(Blob*) blob {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	//draw the blob points
	[[UIColor whiteColor] set];
	for (BlobPoint* blobPoint in blob.blobPoints) {
		CGRect rect = CGRectMake(blobPoint.point.x - 2, blobPoint.point.y - 2, 4, 4);
		CGContextFillEllipseInRect(context, rect);		
	}
}

- (void) drawBezierPoints:(Blob*) blob {
	CGContextRef context = UIGraphicsGetCurrentContext();

	
	for (int i = 0; i < blob.bezierPoints.count; ++i) {
		BezierPoint* bezierPoint = [blob.bezierPoints objectAtIndex:i];
//	for (BezierPoint* bezierPoint in blob.bezierPoints) {
		[[UIColor purpleColor] set];
		int r = 4;
		CGRect rect = CGRectMake(bezierPoint.point.x - r/2, bezierPoint.point.y - r/2, r, r);
		CGContextFillEllipseInRect(context, rect);
		
		[[UIColor whiteColor] set];
		NSString* indexStr = [NSString stringWithFormat:@"%d", i];
		[indexStr drawAtPoint:bezierPoint.point withFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]]; 
		
		[self drawControlPoints:bezierPoint];
	}
}

- (void) drawControlPoints:(BezierPoint*) bezierPoint {
	CGContextRef context = UIGraphicsGetCurrentContext();
	[[UIColor orangeColor] set];
	for (NSValue* cpValue in bezierPoint.controlPoints) {
		int r = 4;
		CGPoint p = [cpValue CGPointValue];
		CGRect rect = CGRectMake(p.x - r/2, p.y - r/2, r, r);
		CGContextFillEllipseInRect(context, rect);
	}
}


- (void)dealloc {
    [super dealloc];
}


@end
