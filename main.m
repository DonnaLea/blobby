//
//  main.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlobbyAppDelegate.h"

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([BlobbyAppDelegate class]));
    [pool release];
    return retVal;
}
