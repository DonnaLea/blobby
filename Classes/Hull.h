//
//  Hull.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LineVector;
@class ExtPoint;

@interface Hull : NSObject {

	NSMutableArray* points; //the points that are passed in to create a hull with
//	LineVector* tempLns;
	LineVector* hull;
	LineVector* tempHull;
	
	NSMutableArray* hullIndices;
}

@property (readonly) NSArray* points;
//@property (readonly) LineVector* tempLns;
@property (readonly) LineVector* hull;
@property (readonly) LineVector* tempHull;
@property (readonly) NSMutableArray* hullIndices;

//constructor
- (id) initWithPoints:(NSArray*) thePoints;

- (void) quickWithPoints:(NSArray*) thePoints andLPoint:(ExtPoint*) l andRPoint:(ExtPoint*) r andFaceDir:(int) faceDir;

//calculate the hull
- (LineVector*) computeQuickHull;

//splits the points
- (int) splitPoints:(NSArray*) thePoints withPoint:(ExtPoint*) l andWithPoint:(ExtPoint*) r;


@end
