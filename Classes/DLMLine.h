//
//  DLMLine.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BlobPoint;

@interface DLMLine : NSObject {
	
	BlobPoint* point1;
	BlobPoint* point2;
	
	BOOL slopeUndefined;
	
	float slope;
	
}

@property (readonly) BlobPoint* point1;
@property (readonly) BlobPoint* point2;
@property BOOL slopeUndefined;
@property float slope;

//constructor
- (id) initWithBlobPoint1:(BlobPoint*) bp1 andBlobPoint2:(BlobPoint*) bp2;

//checks to see if the given point is on the left of this line or not
- (BOOL) onLeftOfCheckPoint:(BlobPoint*) chkpt;


@end
