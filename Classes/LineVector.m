//
//  LineVector.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "LineVector.h"
#import "DLMLine.h"


@implementation LineVector

@synthesize lines;

- (id) init {
	self = [super init];
	if (self) {
		lines = [[NSMutableArray alloc] init];
		[self reset];
	}
	
	return self;
}

//add the line to the end of the list
- (void) addLine:(DLMLine*) l {
	[lines addObject:l];
}

//adds the line to the beginning of the list
- (void) insertLine:(DLMLine*) l {
	[lines insertObject:l atIndex:0];
}

- (void) reset {
	[lines removeAllObjects];
}

//getter
- (int) getLength {
	return [lines count];
}

- (void) dealloc {
	[lines release];
	
	[super dealloc];
}

@end
