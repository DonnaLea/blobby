//
//  LineVector.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DLMLine;

@interface LineVector : NSObject {
	NSMutableArray* lines;
}

@property (readonly) NSArray* lines;

//adds a line to the vector
- (void) addLine:(DLMLine*) l;

//inserts a line
- (void) insertLine:(DLMLine*) l;

//resets the line
- (void) reset;

//gets the length of the vector
- (int) getLength;

@end
