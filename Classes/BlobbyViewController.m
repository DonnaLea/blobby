//
//  BlobbyViewController.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BlobbyViewController.h"
#import "VectorMath.h"
#import "BlobPoint.h"
#import "Blob.h"

float maxWidth;
float maxHeight;
float minWidth;
float minHeight;

@implementation BlobbyViewController

@synthesize blobPoints;
@synthesize blobs;
@synthesize displaySplash;
@synthesize splash;

// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // start with the screen height and width and adjust when we know the actual size in viewDidAppear.
        maxWidth = [UIScreen mainScreen].bounds.size.width;
		maxHeight = [UIScreen mainScreen].bounds.size.height;
		minWidth = 0;
		minHeight = 0;
        
		// Custom initialization
		self.tabBarItem.image = [UIImage imageNamed:@"navbarblob"];
		self.title = @"Blobby";
		
		UIAccelerometer *accel = [UIAccelerometer sharedAccelerometer];
		accel.delegate = self;
		accel.updateInterval = kUpdateInterval;
		
		blobs = [[NSMutableArray alloc] init];

		[self initialise];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    maxWidth = self.view.bounds.size.width;
    maxHeight = self.view.bounds.size.height;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self saveDefaults];

    [super viewWillDisappear:animated];
}

void interruptionListener(	void *	inClientData,
						  UInt32	inInterruptionState) {
	
	
	BlobbyViewController* controller = (BlobbyViewController*)inClientData;
    if (inInterruptionState == kAudioSessionBeginInterruption) {
		if (controller->squishSoundPlayer.playing) {
			[controller->squishSoundPlayer stop];
		}
		
		if (controller->stretchSoundPlayer.playing) {
			[controller->stretchSoundPlayer pause];
		}
	} else if (inInterruptionState == kAudioSessionEndInterruption) {
		//we don't actually have to do anything here to resume audio
	}
	
}

- (void) initialise {
	srandom(time(NULL));
	blobPoints = [[NSMutableArray alloc] init];
	
	[self loadDefaults];

	AudioSessionInitialize( NULL,                    // uses the default run loop
							NULL,                    // uses the default run loop mode
							interruptionListener,    // your callback function (can be set to NULL)
							self                 // data you want sent to your callback							
	);
	
	UInt32 sessionCategory = kAudioSessionCategory_UserInterfaceSoundEffects;
	AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
	
	stretchSoundFilePath = [[NSBundle mainBundle] pathForResource: @"balloon stretch 2" ofType: @"wav"];
	stretchingSoundFileURL = [[NSURL alloc] initFileURLWithPath: stretchSoundFilePath];
	stretchSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: stretchingSoundFileURL error: nil];
	
	[stretchingSoundFileURL release];
	[stretchSoundPlayer prepareToPlay];
	stretchSoundPlayer.numberOfLoops = -1;
	[stretchSoundPlayer setDelegate: self];
	
	squishSoundFilePath = [[NSBundle mainBundle] pathForResource:@"squish" ofType:@"wav"];
	squishSoundFileURL = [[NSURL alloc] initFileURLWithPath:squishSoundFilePath];
	squishSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:squishSoundFileURL error:nil];
	
	[squishSoundFileURL release];
	[squishSoundPlayer prepareToPlay];
	[squishSoundPlayer setDelegate:self];
	
	[self initialiseBlobsFromBlobPoints];
	
	//start run loop?
	NSTimer* theTimer=[NSTimer timerWithTimeInterval:0.03 target:self selector:@selector(update:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:theTimer forMode:NSRunLoopCommonModes];

	
}

- (void) update:(id) sender {
	if (!updating) {
		updating = YES;
		BOOL update = NO;

        minHeight = 0;
		
		for (BlobPoint* blobPoint in blobPoints) {
			if ([blobPoint updatePosition]) {
				update = YES;
			}
		}
		
		if (update) {
			[self initialiseBlobsFromBlobPoints];
		}
	}
	updating = NO;
}

- (void) playStretch {
    // if not already playing
    if (!stretchSoundPlayer.playing) {
		stretchSoundPlayer.currentTime = random() % (int) stretchSoundPlayer.duration;
        [stretchSoundPlayer play];
    }
}

- (void) stopPlayingStretch {
	//if playing, stop playing
	if (stretchSoundPlayer.playing) {
		[stretchSoundPlayer stop];
	}
}

- (void) playSquishSound {
    if (!squishSoundPlayer.playing) {
        [squishSoundPlayer play];
    }
}

- (void) initialiseBlobsFromBlobPoints {

/*
	//Log the details of the current locations of the blob points
	for (int i = 0; i < blobPoints.count; ++i) {
		BlobPoint* blobPoint = [blobPoints objectAtIndex:i];
		NSLog(@"bp[%d]: {%f, %f}", i, blobPoint.point.x, blobPoint.point.y);
	}
/**/
	
	//now figure out which points belong to which Blob (thus creating the Blobs)
	[self distributePointsToBlobs];
	
	//just in case there have been any Blobs left over that don't have any 
	//BlobPoints left in them
	[self removeEmptyBlobs];
	
	//now build each Blob
	for (int i = 0; i < blobs.count; ++i) {
		Blob* blob = [blobs objectAtIndex:i];
		[blob build];
	}
	
	[self.view setNeedsDisplay];
}

- (void) distributePointsToBlobs {
	[blobs removeAllObjects];
	
	[self insertBlobPoint:[blobPoints objectAtIndex:0] intoBlob:nil];
	
	if (blobPoints.count <= 1) {
		return;
	}
	
	int i = 1;
	do {
		BlobPoint* blobPoint = [blobPoints objectAtIndex:i];
		
		Blob* myBlob = nil;
		
		for (int j = 0; j < i; ++j) {
			BlobPoint* bp = [blobPoints objectAtIndex:j];
			
			float distance = [VectorMath distanceBetweenPoint:blobPoint.point andPoint:bp.point];
			float connectionDistance = bp.connectRadius + blobPoint.connectRadius;
			
			//the blob points are close enough to be in the same blob
			if (distance <= connectionDistance) {
				
				//this is the first blob that we've found
				if (myBlob == nil) {
					myBlob = bp.blob; //the blob that j is in
					[self insertBlobPoint:blobPoint intoBlob:myBlob];
				} else {
					//we've already connected with another blob, copy the new blob into the original blob
					Blob* newBlob = bp.blob;
					if (newBlob != myBlob) {
						[self joinBlob:myBlob withBlob:newBlob];
					}
				}
				
			}
		}
		
		//if we weren't able to find any blobs close enough
		if (myBlob == nil) {
			[self insertBlobPoint:blobPoint intoBlob:nil];
		}
		
		++i;
	} while (i < blobPoints.count);	
	
}

- (void) insertBlobPoint:(BlobPoint*) blobPoint intoBlob:(Blob*) blob {
	if (blob == nil) {
		blob = [[Blob alloc] init];
		[blobs addObject:blob];
		[blob autorelease];
	}
	[blob addBlobPoint:blobPoint];
}

- (void) joinBlob:(Blob*) blob1 withBlob:(Blob*) blob2 {
	if (blob1 != nil && blob2 != nil) {
		for (int i = 0; i < blob2.blobPoints.count; ++i) {
			BlobPoint* bp = [blob2.blobPoints objectAtIndex:i];
			[self insertBlobPoint:bp intoBlob:blob1];
		}
		
		[blob2 clearBlobPoints];
	}	
}

- (void) removeEmptyBlobs {
	
	int numBlobs = blobs.count;
	
	for (int i = numBlobs-1; i >= 0; --i) {
		Blob* blob = [blobs objectAtIndex:i];
		if ([blob numBlobPoints] <= 0) {
			//it doesn't have any blob points, we don't want it anymore
			[blobs removeObjectAtIndex:i];
		}
	}
	
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark Touch code
- (void) touchesBegan:(NSSet*) touches withEvent:(UIEvent*) event {
	if (displaySplash) {
		//dismiss the splash
		[self dismissSplash];
	} else {
		UITouch *touch = [touches anyObject];
		NSUInteger tapCount = [touch tapCount];
		switch (tapCount) {
			case 2:
				NSLog(nil);
				BlobPoint* bp = [self findClosestBlobPointToPoint:[touch locationInView:self.view]];
				//delete blob point
				if (bp != nil) {
					if (blobPoints.count > 1) {
						for (int i = 0; i < blobPoints.count; ++i) {
							BlobPoint* tempBp = [blobPoints objectAtIndex:i];
							if (tempBp == bp) {
								[blobPoints removeObjectAtIndex:i];
								[self initialiseBlobsFromBlobPoints];
							}
						}
					}
					//add blob point
				} else {
					bp = [[BlobPoint alloc] initWithPoint:[touch locationInView:self.view]];
					[blobPoints addObject:bp];
					[bp release];
					[self initialiseBlobsFromBlobPoints];
				}
				break;
				
			default:
				//dragging a blob
				blobPointBeingDragged = [self findClosestBlobPointToPoint:[touch locationInView:self.view]];
				if (blobPointBeingDragged != nil) {
					//start dragging the blobPoint
					[blobPointBeingDragged startDrag];
					//stretch the blob
					[self playStretch];
				}
				break;
		}
	}
}

- (void) touchesEnded:(NSSet*) touches withEvent:(UIEvent*) event {
	if (blobPointBeingDragged) {
		[blobPointBeingDragged finishDrag];
	}
	
	[self stopPlayingStretch];
}

- (void) touchesMoved:(NSSet*) touches withEvent:(UIEvent*) event {
	UITouch* touch = [touches anyObject];

	if (blobPointBeingDragged) {
		CGPoint currentPoint = [touch locationInView:self.view];
		float blobRadius = blobPointBeingDragged.blobRadius;
		
		float minX = [BlobbyViewController minWidth] + blobRadius;
		float maxX = [BlobbyViewController maxWidth] - blobRadius;
		float minY = [BlobbyViewController minHeight] + blobRadius;
		float maxY = [BlobbyViewController maxHeight] - blobRadius;
		
		if (currentPoint.x < minX) {
			currentPoint.x = minX;
		} else if (currentPoint.x > maxX) {
			currentPoint.x = maxX;
		}
		
		if (currentPoint.y < minY) {
			currentPoint.y = minY;
		} else if (currentPoint.y > maxY) {
			currentPoint.y = maxY;
		}
		
		//add the previous position of the blob point to the array of previous positions
		[blobPointBeingDragged trackWithCurPos:currentPoint previousPoint:[touch previousLocationInView:self.view]];
		
		//set the blobpoint to the current position
		blobPointBeingDragged.point = currentPoint;
		
		[self initialiseBlobsFromBlobPoints];
		
			
	}
}

- (void) touchesCancelled:(NSSet*) touches withEvent:(UIEvent*) event {

}

- (BlobPoint*) findClosestBlobPointToPoint:(CGPoint) currentPoint {
	BlobPoint* closestBlobPoint = nil;
	float closestDistance = FLT_MAX;
	for (int i = 0; i < blobPoints.count; ++i) {
		BlobPoint* blobPoint = [blobPoints objectAtIndex:i];
		float distance = [VectorMath distanceBetweenPoint:blobPoint.point andPoint:currentPoint];
		
		if (distance < BLOB_RADIUS) {
			if (distance < closestDistance) {
				closestDistance = distance;
				closestBlobPoint = blobPoint;
			}
		}
	}
	
	if (closestDistance < FLT_MAX && closestBlobPoint != nil) {
		return closestBlobPoint;
	} else {
		return nil;
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void) createBlobPoints {
	
	CGRect blobbyViewBounds = CGRectMake(minWidth+EDGE_BUFFER, minHeight+EDGE_BUFFER, maxWidth-EDGE_BUFFER, maxHeight-EDGE_BUFFER);
	
	[blobPoints removeAllObjects];
	
	/**/
	//generate random points on the screen
	int numBlobs = MIN_RANDOM_NUM_BLOB_POINTS + random() % (MAX_RANDOM_NUM_BLOB_POINTS - MIN_RANDOM_NUM_BLOB_POINTS);
	for (int i = 0; i < numBlobs; ++i) {
		CGPoint point = [VectorMath randomPointInRect:blobbyViewBounds];
		BlobPoint* blobPoint = [[BlobPoint alloc] initWithPoint:point];
		[blobPoints addObject:blobPoint];
		[blobPoint release];
	}
	/**/
	
	/*
	
	CGPoint point4 = CGPointMake(153, 244);
	BlobPoint* blobPoint4 = [[BlobPoint alloc] initWithPoint:point4];
	[blobPoints addObject:blobPoint4];
	[blobPoint4 release];
	
	 CGPoint point1 = CGPointMake(153, 150);
	 BlobPoint* blobPoint1 = [[BlobPoint alloc] initWithPoint:point1];
	 [blobPoints addObject:blobPoint1];
	 [blobPoint1 release];
	 
	 CGPoint point2 = CGPointMake(63, 261);
	 BlobPoint* blobPoint2 = [[BlobPoint alloc] initWithPoint:point2];
	 [blobPoints addObject:blobPoint2];
	 [blobPoint2 release];

	CGPoint point3 = CGPointMake(13, 201);
	BlobPoint* blobPoint3 = [[BlobPoint alloc] initWithPoint:point3];
	[blobPoints addObject:blobPoint3];
	[blobPoint3 release];
	
	
	/*
	CGPoint point5 = CGPointMake(63, 261);
	BlobPoint* blobPoint5 = [[BlobPoint alloc] initWithPoint:point5];
	[blobPoints addObject:blobPoint5];
	[blobPoint5 release];
	
	CGPoint point6 = CGPointMake(13, 201);
	BlobPoint* blobPoint6 = [[BlobPoint alloc] initWithPoint:point6];
	[blobPoints addObject:blobPoint6];
	[blobPoint6 release];
	
	CGPoint point7 = CGPointMake(150, 241);
	BlobPoint* blobPoint7 = [[BlobPoint alloc] initWithPoint:point7];
	[blobPoints addObject:blobPoint7];
	[blobPoint7 release];
	
	*/
}

- (void) loadDefaults {
	
	NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
	int numberOfBlobPoints = [userDefaults integerForKey:@"BlobbyNumOfBlobPoints"];
	if (numberOfBlobPoints > 0) {
		NSArray* defaultBlobPoints = [userDefaults arrayForKey:@"BlobbyBlobPoints"];

		for (int i = 0; i < numberOfBlobPoints; i+=2) {
			NSNumber* number1 = [defaultBlobPoints objectAtIndex:i];
			NSNumber* number2 = [defaultBlobPoints objectAtIndex:i+1];
			CGPoint point = CGPointMake(number1.floatValue, number2.floatValue);
			BlobPoint* blobPoint = [[BlobPoint alloc] initWithPoint:point];
			[blobPoints addObject:blobPoint];
			[blobPoint release];
		}
	}
	else { //nothing saved
		[self createBlobPoints];
	}
	
}

- (void) saveDefaults {
	NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray* points = [[NSMutableArray alloc] init];
	
	for (BlobPoint* blobPoint in blobPoints) {
		NSNumber* number1 = [NSNumber numberWithFloat:blobPoint.point.x];
		NSNumber* number2 = [NSNumber numberWithFloat:blobPoint.point.y];
		[points addObject:number1];
		[points addObject:number2];
	}
	
	int numberOfBlobPoints = [points count];
	
	[userDefaults setInteger:numberOfBlobPoints forKey:@"BlobbyNumOfBlobPoints"];

	[userDefaults setObject:points forKey:@"BlobbyBlobPoints"];
	
	[points release];
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	if (fabsf(acceleration.x) > kAccelerationThreshold || fabsf(acceleration.y) > kAccelerationThreshold || fabsf(acceleration.z) > kAccelerationThreshold) {
		[self playSquishSound];
		[self createBlobPoints];
		[self initialiseBlobsFromBlobPoints];
	}
}

- (void) audioPlayerEndInterruption: (AVAudioPlayer *) player {

}

- (void)dealloc {
	[blobPoints release];
	[blobs release];
	
    [super dealloc];
}

+ (float) maxWidth {
	return maxWidth;
}

+ (float) maxHeight {
	return maxHeight;
}

+ (float) minWidth {
	return minWidth;
}

+ (float) minHeight {
	return minHeight;
}

@end
