//
//  Blob.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Blob.h"
#import "Hull.h"
#import "BezierPoint.h"
#import "BlobPoint.h"
#import "VectorMath.h"

@implementation Blob

@synthesize blobPoints;
@synthesize bezierPoints;
@synthesize hull;
@synthesize path;
@synthesize colour;

- (id) init {
	self = [super init];
	if (self) {
		hull = nil;
		path = nil;
		bezierPoints = [[NSMutableArray alloc] init];
		blobPoints = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (void) dealloc {
	[bezierPoints release];
	[hull release];
	[blobPoints release];
	if (path != nil) {
		CGPathRelease(path);
		path = nil;
	}
	[colour release];
	
	[super dealloc];
}

- (void) calculateColour {
	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	
	for (BlobPoint* blobPoint in blobPoints) {
		red += blobPoint.redColour/255.0f;
		green += blobPoint.greenColour/255.0f;
		blue += blobPoint.blueColour/255.0f;
	}
	
	red /= blobPoints.count;
	green /= blobPoints.count;
	blue /= blobPoints.count;
	
	self.colour = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

- (void) build {
	
	//we don't need the reference to the hull anymore;
	[hull release];
	
	//get ready to track the blob points
	hull = [[Hull alloc] initWithPoints:blobPoints];
	
	[hull computeQuickHull];
	
	[self generateBezierPoints];
	
	[self generateControlPoints];
	
	[self generatePath];
	
	//calculate colour
	[self calculateColour];
	
}

- (void) generatePath {
	
	/**/
	
	// Loop through points to draw cubic Bezier curves through the penultimate point, or through the last point if the line is closed.
	BezierPoint* bezierPoint = [bezierPoints objectAtIndex:0];
	BezierPoint* nextBezierPoint = nil;
	CGPoint point = bezierPoint.point;

	if (path != nil) {
		CGPathRelease(path);
		path = nil;
	}
	
	path = CGPathCreateMutable();
	
	CGPathMoveToPoint(path, NULL, point.x, point.y);
	
	for (int i = 0; i < [bezierPoints count] - 1; ++i) {
		bezierPoint = [bezierPoints objectAtIndex:i];
		nextBezierPoint = [bezierPoints objectAtIndex:i+1];
		
		CGPoint nextPoint = nextBezierPoint.point;

		NSValue* cp2 = [bezierPoint.controlPoints objectAtIndex:1];
		NSValue* cp1 = [nextBezierPoint.controlPoints objectAtIndex:0];
		
		CGPoint controlPoint2 = [cp2 CGPointValue];
		CGPoint controlPoint1 = [cp1 CGPointValue];
		CGPathAddCurveToPoint(path, NULL, controlPoint2.x, controlPoint2.y, controlPoint1.x, controlPoint1.y, nextPoint.x, nextPoint.y);
	}
	
	//curve back to the first point
	bezierPoint = [bezierPoints lastObject];
	nextBezierPoint = [bezierPoints objectAtIndex:0];

	NSValue* cp2 = [bezierPoint.controlPoints objectAtIndex:1];
	CGPoint controlPoint2 = [cp2 CGPointValue];
	NSValue* cp1 = [nextBezierPoint.controlPoints objectAtIndex:0];
	CGPoint controlPoint1 = [cp1 CGPointValue];
	CGPathAddCurveToPoint(path, NULL, controlPoint2.x, controlPoint2.y, controlPoint1.x, controlPoint1.y, nextBezierPoint.point.x, nextBezierPoint.point.y);
	
	CGPathCloseSubpath(path);
	/**/
	
}

- (int) numBlobPoints {
	return [blobPoints count];
}

- (void) addBlobPoint:(BlobPoint*) bp {
	//check if this blobpoint already exists in this blob
	if (![self alreadyHasBlobPoint:bp]) {
		[blobPoints addObject:bp];
		bp.blob = self;
	}
}

- (BOOL) alreadyHasBlobPoint:(BlobPoint*) blobPoint {
	bool contains = false;
	for (unsigned int i = 0; i < blobPoints.count; ++i) {
		BlobPoint* bp = [blobPoints objectAtIndex:i];
		if (bp.point.x == blobPoint.point.x && bp.point.y == blobPoint.point.y) {
			contains = true;
			break;
		}
	}
	
	return contains;
	
}

- (void) clearBlobPoints {
	[blobPoints removeAllObjects];
}

- (void) generateBezierPoints {
	
	if (hull.hullIndices.count == 1) {
		NSNumber* ind  = [hull.hullIndices objectAtIndex:0];
		int index = [ind intValue];
		BlobPoint* pt0 = [blobPoints objectAtIndex:index];
		
		BezierPoint* bezierPoint1 = [self createBezierPointWithX:pt0.point.x andY:pt0.point.y andAngle:0 andRadius:BLOB_RADIUS];
		BezierPoint* bezierPoint2 = [self createBezierPointWithX:pt0.point.x andY:pt0.point.y andAngle:M_PI/2 andRadius:BLOB_RADIUS];
		BezierPoint* bezierPoint3 = [self createBezierPointWithX:pt0.point.x andY:pt0.point.y andAngle:M_PI andRadius:BLOB_RADIUS];
		BezierPoint* bezierPoint4 = [self createBezierPointWithX:pt0.point.x andY:pt0.point.y andAngle:M_PI + M_PI/2 andRadius:BLOB_RADIUS];
		BezierPoint* bezierPoint5 = [self createBezierPointWithX:pt0.point.x andY:pt0.point.y andAngle:0 andRadius:BLOB_RADIUS];
		
		[bezierPoints addObject:bezierPoint1];
		[bezierPoints addObject:bezierPoint2];
		[bezierPoints addObject:bezierPoint3];
		[bezierPoints addObject:bezierPoint4];
		[bezierPoints addObject:bezierPoint5];
		
		return;
	}
	
	
	int i = 0;
	NSMutableArray* hullIndices = hull.hullIndices;
	int numHullIndices = hullIndices.count;
	int limit = numHullIndices - 1;
	if (numHullIndices == 2) {
		limit = 0;
	}
	
	BlobPoint* hullPoint1;
	BlobPoint* hullPoint2;
	float angle;
	do {
		NSNumber* ind = [hullIndices objectAtIndex:i];
		int index1 = [ind intValue];
		ind = [hullIndices objectAtIndex:i+1];
		int index2 = [ind intValue];
		hullPoint1 = [blobPoints objectAtIndex:index1];
		hullPoint2 = [blobPoints objectAtIndex:index2];
		
		
		float dx = hullPoint2.point.x - hullPoint1.point.x;
		float dy = hullPoint2.point.y - hullPoint1.point.y;
		angle = (float) atan2(dy, dx);
		
		float distance = sqrt(dx*dx + dy*dy);
		
		[bezierPoints addObject:[self createBezierPointWithX:hullPoint1.point.x andY:hullPoint1.point.y andAngle:angle + M_PI/2 andRadius:BLOB_RADIUS]];
		volatile float radius2 = BLOB_RADIUS - distance/6.0f;
		
		if (radius2 < 0.0f) {
			radius2 = 0.0f;
		}
		
		[bezierPoints addObject:[self createBezierPointWithX:(hullPoint1.point.x + hullPoint2.point.x)/2 andY:(hullPoint1.point.y + hullPoint2.point.y)/2 
													andAngle:angle + M_PI/2 andRadius:radius2]];
		
		[bezierPoints addObject:[self createBezierPointWithX:hullPoint2.point.x andY:hullPoint2.point.y andAngle:angle + M_PI/2 andRadius:BLOB_RADIUS]];
		
//		[bezierPoints addObject:[self createBezierPointWithX:hullPoint2.point.x andY:hullPoint2.point.y andAngle:angle + M_PI/2 andRadius:BLOB_RADIUS]];

		++i;
		//we want to use the last iteration of these values
//		if (i < limit) {
//			[hullPoint1 release];
//			[hullPoint2 release];
//		}
	} while(i < limit);
	

//	bezierPoints.push_back(new BezierPoint(*bezierPoints[0]));
//	[bezierPoints addObject:[bezierPoints objectAtIndex:0]];
	BezierPoint* tempBP = [[BezierPoint alloc] initWithBezierPoint:[bezierPoints objectAtIndex:0]];
	[bezierPoints addObject:tempBP];
	[tempBP release];
	
	if (numHullIndices == 3) {
		[bezierPoints insertObject:[self createBezierPointWithX:hullPoint2.point.x andY:hullPoint2.point.y andAngle:angle andRadius:BLOB_RADIUS] atIndex:6];
		[bezierPoints insertObject:[self createBezierPointWithX:hullPoint1.point.x andY:hullPoint1.point.y andAngle:angle - M_PI andRadius:BLOB_RADIUS] atIndex:3];
	}
	
	BezierPoint* bp1 = [bezierPoints objectAtIndex:0];
	BezierPoint* bp2 = [bezierPoints lastObject];
	if (bp1.point.x != bp2.point.x || bp1.point.y != bp2.point.y) {		
		NSLog(nil);
//		NSLog(@"POTENTIAL BREAK");
	}
	
}

- (BezierPoint*) createBezierPointWithX:(float) x andY:(float) y andAngle:(float) angle andRadius:(float) radius {
	CGPoint p = CGPointMake(x + radius*cos(angle), y + radius*sin(angle));
	BezierPoint* bezierPoint = [[BezierPoint alloc] initWithPoint:p];
	[bezierPoint autorelease];
	return bezierPoint;	
}

- (void) generateControlPoints {
	float z = 0.5;
	float angleFactor = 0.75;
	
	NSMutableArray* p = bezierPoints;
	NSMutableArray* duplicates = [[NSMutableArray alloc] init]; // Array to hold indices of duplicate points
	
	for (int i = 0; i < p.count; ++i) {
		// Check for the same point twice in a row
		if (i > 0) {
			BezierPoint* bezierPoint = [[p objectAtIndex:i] retain];
			CGPoint point = bezierPoint.point;
			
			BezierPoint* prevBezierPoint = [[p objectAtIndex:i-1] retain];
			CGPoint prevPoint = prevBezierPoint.point;
			if (point.x == prevPoint.x && point.y == prevPoint.y) {
				[duplicates addObject:[NSNumber numberWithInt:i]]; // add index of duplicate to duplicates array
			}
			[bezierPoint release];
			[prevBezierPoint release];
		}
	}
	
	// Loop through duplicates array and remove points that were flagged in the duplicates array
	for (int i = duplicates.count - 1; i >= 0; --i) {
		NSNumber* ind = [duplicates objectAtIndex:i];
		int index = [ind intValue];
		[p removeObjectAtIndex:index];
	}
	
	//finished with the duplicates
	[duplicates release];
	
	// Make sure z is between 0 and 1 (too messy otherwise)
	if (z <= 0) {
		z = 0.5;
	} else if (z > 1) {
		z = 1;
	}
	// Make sure angleFactor is between 0 and 1
	if (angleFactor < 0) {
		angleFactor = 0;
	} else if (angleFactor > 1) {
		angleFactor = 1;
	}
	
	//
	// First calculate all the curve control points
	//
	
	// None of this junk will do any good if there are only two points
	if (p.count > 2) {
		// Ordinarily, curve calculations will start with the second point and go through the second-to-last point
		int pSize = (int) p.count;
		int firstPtIndex = 1;
		int lastPtIndex = pSize - 1;
		
		// Check if this is a closed line (the first and last points are the same)
		BezierPoint* firstBp = [p objectAtIndex:0];
		BezierPoint* lastBp = [p lastObject];
		if (firstBp.point.x == lastBp.point.x && firstBp.point.y == lastBp.point.y) {
			// Include first and last points in curve calculations
			firstPtIndex = 0;
			lastPtIndex = pSize;
		} else {
			NSLog(nil);
//			NSLog(@"POTENTIAL BREAK");
		}
		
		// Loop through all the points (except the first and last if not a closed line) to get curve control points for each.
		for (int i = firstPtIndex; i < lastPtIndex; ++i) {
			
			// The previous, current, and next points
			int firstIdx = (i - 1 < 0) ? pSize - 2 : i - 1; // If the first point (of a closed line), use the second-to-last point as the previous point
			BezierPoint* bp0 = [p objectAtIndex:firstIdx];
			CGPoint p0 = bp0.point;
			BezierPoint* bp1 = [p objectAtIndex:i];
			CGPoint p1 = bp1.point;
			int thirdIdx = (i + 1 == pSize) ? 1 : i + 1; // If the last point (of a closed line), use the second point as the next point
			BezierPoint* bp2 = [p objectAtIndex:thirdIdx];
			CGPoint p2 = bp2.point;
			float a = [VectorMath distanceBetweenPoint:p0 andPoint:p1]; // Distance from previous point to current point
			if (a < 0.001f) {
				a = 0.001f; // Correct for near-zero distances, which screw up the angles or something
			}
			float b = [VectorMath distanceBetweenPoint:p1 andPoint:p2]; // Distance from current point to next point
			if (b < 0.001f) {
				b = 0.001f; // Correct for near-zero distances, which screw up the angles or something
			}
			float c = [VectorMath distanceBetweenPoint:p0 andPoint:p2]; // Distance from previous point to next point
			if (c < 0.001f) {
				c = 0.001f;
			}
			float angleC = acos((b*b +a*a - c*c)/(2*b*a)); // Angle formed by the two sides of the triangle (described by the three points above) adjacent to the current point
			
			// Duplicate set of points. Start by giving previous and next points values RELATIVE to the current point.
			CGPoint aPt = CGPointMake(p0.x-p1.x,p0.y-p1.y);
			CGPoint bPt = CGPointMake(p1.x,p1.y);
			CGPoint cPt = CGPointMake(p2.x-p1.x,p2.y-p1.y);
			/*
			 We'll be adding adding the vectors from the previous and next points to the current point,
			 but we don't want differing magnitudes (i.e. line segment lengths) to affect the direction
			 of the new vector. Therefore we make sure the segments we use, based on the duplicate points
			 created above, are of equal length. The angle of the new vector will thus bisect angle C
			 (defined above) and the perpendicular to this is nice for the line tangent to the curve.
			 The curve control points will be along that tangent line.
			 */
			if (a > b) {
				// Scale the segment to aPt (bPt to aPt) to the size of b (bPt to cPt) if b is shorter.
				aPt = [VectorMath normalizeVector:aPt];
				aPt = [VectorMath scaleVector:aPt withScale:b];
			} else if (b > a) {
				// Scale the segment to cPt (bPt to cPt) to the size of a (aPt to bPt) if a is shorter.
				cPt = [VectorMath normalizeVector:cPt];
				cPt = [VectorMath scaleVector:cPt withScale:a];
			}
			
			// Offset aPt and cPt by the current point to get them back to their absolute position.
			aPt = [VectorMath addVector:aPt withVector:p1];
			cPt = [VectorMath addVector:cPt withVector:p1];
			
			// Get the sum of the two vectors, which is perpendicular to the line along which our curve control points will lie.
			float ax = bPt.x-aPt.x;	// x component of the segment from previous to current point
			float ay = bPt.y-aPt.y; 
			float bx = bPt.x-cPt.x;	// x component of the segment from next to current point
			float by = bPt.y-cPt.y;
			float rx = ax + bx;	// sum of x components
			float ry = ay + by;
			
			float theta = atan(ry/rx);	// angle of the new vector
			
			float controlDist; // = min(a,b)*z;	// Distance of curve control points from current point: a fraction the length of the shorter adjacent triangle side
			if (a < b) {
				controlDist = a * z;
			} else {
				controlDist = b * z;
			}
			float controlScaleFactor = angleC/M_PI;	// Scale the distance based on the acuteness of the angle. Prevents big loops around long, sharp-angled triangles.
			controlDist = controlDist * ((1-angleFactor) + angleFactor*controlScaleFactor);	// Mess with this for some fine-tuning
			float controlAngle = theta+M_PI/2;	// The angle from the current point to control points: the new vector angle plus 90 degrees (tangent to the curve).
			
			CGPoint controlPoint2 = [VectorMath convertPolarPointWithDistance:controlDist andAngle:controlAngle]; // Control point 2, curving to the next point.
			CGPoint controlPoint1 = [VectorMath convertPolarPointWithDistance:controlDist andAngle:controlAngle+M_PI]; // Control point 1, curving from the previous point (180 degrees away from control point 2).
			// Offset control points to put them in the correct absolute position
			controlPoint1 = [VectorMath addVector:controlPoint1 withVector:p1];
			controlPoint2 = [VectorMath addVector:controlPoint2 withVector:p1];
			
			/*
			 Haven't quite worked out how this happens, but some control points will be reversed.
			 In this case controlPoint2 will be farther from the next point than controlPoint1 is.
			 Check for that and switch them if it's true.
			 */
			if ([VectorMath distanceBetweenPoint:controlPoint2 andPoint:p2] > [VectorMath distanceBetweenPoint:controlPoint1 andPoint:p2]) {
				// Add the two control points to the array in reverse order
				[bp1 setControlPoint1:controlPoint2 andControlPoint2:controlPoint1];
			} else {
				// Otherwise add the two control points to the array in normal order
				[bp1 setControlPoint1:controlPoint1 andControlPoint2:controlPoint2];
			}
		}	
	}
}

- (NSString*) description {
	NSMutableString* description = [[NSMutableString alloc] init];
	
	for (BlobPoint* blobPoint in blobPoints) {
		[description appendFormat:@"blobpoint: %@\n", blobPoint];
	}

	[description autorelease];
	
	return description;
}

@end
