//
//  BlobbyAppDelegate.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "BlobbyAppDelegate.h"
#import "BlobbyViewController.h"
#import "Blobby-Swift.h"

static NSString* const TestScreen1 = @"01Blobby5ScreenTest";
static NSString* const TestScreen2 = @"02Blobby4ScreenTest";
static NSString* const TestScreen3 = @"03Blobby3ScreenTest";
static NSString* const TestScreen4 = @"04BlobbyMultiScreenTest";

static NSString* const BLOB_POINTS_KEYS = @"BlobbyBlobPoints";
static NSString* const NUM_POINTS_KEY = @"BlobbyNumOfBlobPoints";

@implementation BlobbyAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(UIApplication *)application {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

	tabBarController = [[UITabBarController alloc] init];
	
	blobbyViewController = [[BlobbyViewController alloc] initWithNibName:@"BlobbyView" bundle:nil];
	
	ControlsViewController* controlsViewController = [[ControlsViewController alloc] init];
    tabBarController.viewControllers = @[blobbyViewController, controlsViewController];

    // UI testing setup.
    NSArray* arguments = [[NSProcessInfo processInfo] arguments];
    if ([arguments containsObject:TestScreen1]) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:10 forKey:NUM_POINTS_KEY];
        NSArray* points = @[@73.62909, @229.62, @111.0347, @354.625, @161.9325, @131.05, @279.0667, @346.9, @266.575, @202.475];
        [userDefaults setObject:points forKey:BLOB_POINTS_KEYS];
        [userDefaults synchronize];
    } else if ([arguments containsObject:TestScreen2]) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:8 forKey:NUM_POINTS_KEY];
        NSArray* points = @[@73.62909, @229.62, @211.06, @306.34, @161.9325, @131.05, @266.575, @202.475];
        [userDefaults setObject:points forKey:BLOB_POINTS_KEYS];
        [userDefaults synchronize];
    } else if ([arguments containsObject:TestScreen3]) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:22 forKey:NUM_POINTS_KEY];
        NSArray* points = @[@129.1143, @265.35, @212.5833, @125.625, @292.45, @255, @330, @508, @54.5, @532.5, @109.5, @453.5, @290.5, @471, @277, @526.5, @212.5, @303, @308.3, @162.05, @61.5, @92.5];
        [userDefaults setObject:points forKey:BLOB_POINTS_KEYS];
        [userDefaults synchronize];
    } else if ([arguments containsObject:TestScreen4]) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:6 forKey:NUM_POINTS_KEY];
        NSArray* points = @[@129.1143, @265.35, @161.9325, @131.05, @257.2, @230.3];
        [userDefaults setObject:points forKey:BLOB_POINTS_KEYS];
        [userDefaults synchronize];
    }
	
    // Override point for customization after application launch
    window.rootViewController = tabBarController;
    [window makeKeyAndVisible];
}

//save the application state in here
- (void)applicationWillTerminate:(UIApplication *)application {
	[blobbyViewController saveDefaults];
}

- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
