//
//  BlobPoint.h
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEFAULT_DECELERATION -0.1
#define EXTRA_BLOB_DECELERATION -0.015
#define NUM_DELTAS 5
#define MAX_VELOCITY 20
#define EDGE_VELOCITY 2

@class Blob;

@interface BlobPoint : NSObject {
	CGPoint point;
	int connectRadius;
	Blob* blob;
	CGPoint velocity;
	CGPoint acceleration;
	BOOL beingDragged;
	float blobRadius;
	
	//defines a colour for this one blob point
	NSInteger redColour;
	NSInteger greenColour;
	NSInteger blueColour;

	
	NSMutableArray* positionDeltas;
}
	//constructor
- (id) initWithPoint:(CGPoint) p;
- (BOOL) updatePosition;
- (void) calculateVelocity;
- (void) calculateAcceleration;
- (void) trackWithCurPos:(CGPoint) curPoint previousPoint:(CGPoint) prevPoint;
- (void) startDrag;
- (void) finishDrag;

@property CGPoint point;	
@property (readonly) int connectRadius;
@property (retain) Blob* blob;
@property CGPoint velocity;
@property CGPoint acceleration;
@property (readonly) NSMutableArray* positionDeltas;
@property BOOL beingDragged;
@property (readonly) float blobRadius;
@property (nonatomic) NSInteger redColour;
@property (nonatomic) NSInteger greenColour;
@property (nonatomic) NSInteger blueColour;

@end
