//
//  BezierPoint.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BezierPoint : NSObject {

	CGPoint point;
	NSMutableArray* controlPoints;
	
}

@property (readonly) CGPoint point;
@property (readonly) NSMutableArray* controlPoints;

//constructor
- (id) initWithPoint:(CGPoint) p;

//copy constructor
- (id) initWithBezierPoint:(BezierPoint*) bp;

- (void) setControlPoint1:(CGPoint) point1 andControlPoint2:(CGPoint) point2;

@end
