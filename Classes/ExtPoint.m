//
//  ExtPoint.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ExtPoint.h"


@implementation ExtPoint

@synthesize iD;

- (id) initWithPoint:(CGPoint) p andID:(int) theID {
	self = [super init];
	if (self) {
		self.point = p;
		self.iD = theID;		
	}
	return self;
}

@end
