//
//  ExtPoint.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlobPoint.h"

@interface ExtPoint : BlobPoint {
	int iD;
}

//constructor
- (id) initWithPoint:(CGPoint) p andID:(int) theID;

@property int iD;

@end
