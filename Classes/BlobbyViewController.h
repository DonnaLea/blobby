//
//  BlobbyViewController.h
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@class BlobPoint;
@class Blob;

#define MAX_RANDOM_NUM_BLOB_POINTS 10
#define MIN_RANDOM_NUM_BLOB_POINTS 3
#define EDGE_BUFFER 20
#define kAccelerationThreshold		2.2
#define kUpdateInterval			(1.0f/10.0f)

@interface BlobbyViewController : UIViewController <AVAudioPlayerDelegate, UIAccelerometerDelegate> {
	NSMutableArray* blobPoints;
	NSMutableArray* blobs;
	
	BlobPoint* blobPointBeingDragged;
	
	NSURL* stretchingSoundFileURL;
	NSURL* squishSoundFileURL;
	NSString* stretchSoundFilePath;
	NSString* squishSoundFilePath;
	AVAudioPlayer* stretchSoundPlayer;
	AVAudioPlayer* squishSoundPlayer;
	BOOL updating;
	BOOL displaySplash;
	
	UIImage* splash;
	
	NSTimer *refreshTimer; // timer to get fresh ads
}

- (void) initialise;
- (void) initialiseBlobsFromBlobPoints;
- (void) distributePointsToBlobs;
- (void) insertBlobPoint:(BlobPoint*) blobPoint intoBlob:(Blob*) blob;
- (void) joinBlob:(Blob*) blob1 withBlob:(Blob*) blob2;
- (void) removeEmptyBlobs;
- (BlobPoint*) findClosestBlobPointToPoint:(CGPoint) currentPoint;
- (void) playStretch;
- (void) stopPlayingStretch;
- (void) loadDefaults;
- (void) saveDefaults;
- (void) update:(id) sender;

+ (float) maxWidth;
+ (float) maxHeight;
+ (float) minWidth;
+ (float) minHeight;
//- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration;
	
@property (readonly) NSMutableArray* blobPoints;
@property (readonly) NSMutableArray* blobs;
@property (readonly) BOOL displaySplash;
@property (readonly) UIImage* splash;

@end
