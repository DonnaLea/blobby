//
//  BlobbyAppDelegate.h
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BlobbyViewController;

@interface BlobbyAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	UITabBarController* tabBarController;
	BlobbyViewController* blobbyViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

