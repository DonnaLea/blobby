//
//  BezierPoint.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BezierPoint.h"


@implementation BezierPoint

@synthesize point;
@synthesize controlPoints;

- (id) initWithPoint:(CGPoint) p {
	self = [super init];
	if (self) {
		point = p;
		controlPoints = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (id) initWithBezierPoint:(BezierPoint*) bp {
	self = [super init];
	if (self) {
		point = bp.point;
		controlPoints = [bp.controlPoints retain];
	}
	
	return self;
}

- (void) setControlPoint1:(CGPoint) point1 andControlPoint2:(CGPoint) point2 {

	//clear any possible current control points
	[controlPoints removeAllObjects];
	[controlPoints addObject:[NSValue valueWithCGPoint:point1]];
	[controlPoints addObject:[NSValue valueWithCGPoint:point2]];
	
}

- (void) dealloc {
	[controlPoints release];
	
	[super dealloc];
}

@end
