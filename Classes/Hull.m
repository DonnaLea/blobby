//
//  Hull.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Hull.h"
#import "LineVector.h"
#import "ExtPoint.h"
#import "DLMLine.h"

@implementation Hull

@synthesize points;
//@synthesize tempLns;
@synthesize hull;
@synthesize tempHull;
@synthesize hullIndices;

- (id) initWithPoints:(NSArray*) thePoints {
	
	self = [super init]; 
	if (self) {
			
//		tempLns = [[LineVector alloc] init];
		tempHull = [[LineVector alloc] init];
		hull = [[LineVector alloc] init];
		hullIndices = [[NSMutableArray alloc] init];
		
		points = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < [thePoints count]; ++i) {
			BlobPoint* curBlobPoint = [thePoints objectAtIndex:i];
			ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:[curBlobPoint point] andID:i];
			[points addObject:newPoint];
			
			//we no longer need the reference to newPoint, the array is holding onto it for us
			[newPoint release];
		}
	}
	
	return self;
}

- (void) quickWithPoints:(NSArray*) thePoints andLPoint:(ExtPoint*) l andRPoint:(ExtPoint*) r andFaceDir:(int) faceDir {
	
	if ([thePoints count] == 2) {
		if (faceDir == 0) {
			DLMLine* newLine = [[DLMLine alloc] initWithBlobPoint1:[thePoints objectAtIndex:0] andBlobPoint2:[thePoints objectAtIndex:1]];
			[hull addLine:newLine];
			[newLine release];
			
			ExtPoint* point = [thePoints objectAtIndex:1];
			[hullIndices addObject:[NSNumber numberWithInt:point.iD]];
		} else {
			DLMLine* newLine = [[DLMLine alloc] initWithBlobPoint1:[thePoints objectAtIndex:0] andBlobPoint2:[thePoints objectAtIndex:1]];
			[hull addLine:newLine];
			//we no longer need the reference to this line
			[newLine release];
			
			ExtPoint* tempPoint = [thePoints objectAtIndex:1];
			[hullIndices insertObject:[NSNumber numberWithInt:tempPoint.iD] atIndex:0];
		}
		return;
	} else {
		unsigned int hAt = [self splitPoints:thePoints withPoint:l andWithPoint:r];
		DLMLine* lh = [[DLMLine alloc] initWithBlobPoint1:l andBlobPoint2:[thePoints objectAtIndex:hAt]];
		DLMLine* hr = [[DLMLine alloc] initWithBlobPoint1:[thePoints objectAtIndex:hAt] andBlobPoint2:r];
		
		NSMutableArray* P1 = [[NSMutableArray alloc] init];
		NSMutableArray* P2 = [[NSMutableArray alloc] init];
		
		int i = 0;
		do {
			if (i != hAt) {
				ExtPoint* currPt = [[thePoints objectAtIndex:i] retain];
				if (faceDir == 0) {
					if ([lh onLeftOfCheckPoint:currPt]) {
						ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:currPt.point andID:currPt.iD];
						[P1 addObject:newPoint];
						[newPoint release];
						newPoint = nil;
					}
					
					if ([hr onLeftOfCheckPoint:currPt]) {
						ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:currPt.point andID:currPt.iD];
						[P2 addObject:newPoint];
						[newPoint release];
						newPoint = nil;
					}
				} else {
					if (![lh onLeftOfCheckPoint:currPt]) {
						ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:currPt.point andID:currPt.iD];
						[P1 addObject:newPoint];
						[newPoint release];
						newPoint = nil;
					}
					if (![hr onLeftOfCheckPoint:currPt]) {
						ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:currPt.point andID:currPt.iD];
						[P2 addObject:newPoint];
						[newPoint release];
						newPoint = nil;
					}
				}
				//no longer need that reference
				[currPt release];
				currPt = nil;
			}
			i++;
		} while (i < [thePoints count] - 2);
		
		ExtPoint* hAtPoint = [thePoints objectAtIndex:hAt];
		ExtPoint* h = [[ExtPoint alloc] initWithPoint:hAtPoint.point andID:hAtPoint.iD];
		
		ExtPoint* newPoint = [[ExtPoint alloc] initWithPoint:l.point andID:l.iD];
		[P1 addObject:newPoint];
		[P1 addObject:h];
		[newPoint release];
		newPoint = nil;

		[P2 addObject:h];
		newPoint = [[ExtPoint alloc] initWithPoint:r.point andID:r.iD];
		[P2 addObject:newPoint];
		[newPoint release];
		newPoint = nil;
		
		if (faceDir == 0) {
			[self quickWithPoints:P1 andLPoint:l andRPoint:h andFaceDir:0];
			[self quickWithPoints:P2 andLPoint:h andRPoint:r andFaceDir:0];
		} else {
			[self quickWithPoints:P1 andLPoint:l andRPoint:h andFaceDir:1];
			[self quickWithPoints:P2 andLPoint:h andRPoint:r andFaceDir:1];
		}

		[lh release];
		[hr release];
		[h release];
		
		[P1 release];
		[P2 release];
		
		return;
	}
	
}

- (LineVector*) computeQuickHull {
	//we're not interested in what's in those pointers anymore
//	[tempLns release];
	[hull release];
	
	//tempLns = [[LineVector alloc] init];
	hull = [[LineVector alloc] init];
	[hullIndices removeAllObjects];
	
	if ([points count] == 1) {
		[hullIndices addObject:[NSNumber numberWithInt:0]];
		return hull;
	}
	
	NSMutableArray* points1 = [[NSMutableArray alloc] init];
	NSMutableArray* points2 = [[NSMutableArray alloc] init];
	
	ExtPoint* l = [points objectAtIndex:0];
	ExtPoint* r = [points objectAtIndex:0];
	
	float minX = l.point.x;
	float maxX = l.point.x;
	unsigned int minAt = 0;
	unsigned int maxAt = 0;
	
	
	// find the max and min x-coord point
	unsigned int i = 1;
	do {
		ExtPoint* currPt = [points objectAtIndex:i];
		if (currPt.point.x >= maxX) {
			r = currPt;
			maxX = currPt.point.x;
			maxAt = i;
		}
		if (currPt.point.x < minX) {
			l = currPt;
			minX = currPt.point.x;
			minAt = i;
		}
		
		i++;
	} while (i < [points count]);
	
	DLMLine* lr = [[DLMLine alloc] initWithBlobPoint1:l andBlobPoint2:r];
	DLMLine* newLine = [[DLMLine alloc] initWithBlobPoint1:[points objectAtIndex:maxAt] andBlobPoint2:[points objectAtIndex:minAt]];
	//[tempLns addLine:newLine];
	[newLine release];
	newLine = nil;
	
	// find out each point is over or under the line formed by the two points 
	// with min and max x-coord, and put them in 2 groups according to whether 
	// they are above or under
	i = 0;
	do {
		if ((i != maxAt) && (i != minAt)) {
			ExtPoint*  currPt = [[points objectAtIndex:i] retain];
			
			if ([lr onLeftOfCheckPoint:currPt]) {
				[points1 addObject:currPt];
			} else {
				[points2 addObject:currPt];
			}
			
			//we no longer need this pointer
			[currPt release];
		}     			
		i++;
	} while (i < [points count]);
	
	// put the max and min x-cord points in each group 
	[points1 addObject:l];
	[points1 addObject:r];
	[points2 addObject:l];
	[points2 addObject:r];
	
	// calculate the upper hull
	[self quickWithPoints:points1 andLPoint:l andRPoint:r andFaceDir:0];
	
	[hullIndices insertObject:[NSNumber numberWithInt:l.iD] atIndex:0];
	
	// calculate the lower hull
	[self quickWithPoints:points2 andLPoint:l andRPoint:r andFaceDir:1];
	
	[lr release];
	[points1 release];
	[points2 release];
	
	
	//check if there are duplicate points in the hullindices (except if it is the first/last object)
	BOOL* exists = calloc(sizeof(BOOL), [[self points] count]);
	memset(exists, NO, sizeof(BOOL)*[[self points] count]);
	for(int i = 1; i < [hullIndices count] - 1; ++i) {
		NSNumber* number = [hullIndices objectAtIndex:i];
		int index = [number intValue];
		if (exists[index] == YES) {
			//it already exists, remove it from the hull indices list
			[hullIndices removeObjectAtIndex:i];
			--i;
		} else {
			exists[index] = YES;
		}
	}
			
	free(exists);
	
	
	return hull;				
}

- (int) splitPoints:(NSArray*) thePoints withPoint:(ExtPoint*) l andWithPoint:(ExtPoint*) r {
	float maxDist = 0;
	DLMLine* newLn = [[DLMLine alloc] initWithBlobPoint1:l andBlobPoint2:r];
	
	float x3 = 0;
	float y3 = 0;
	float distance = 0;
	int farPt = 0;
	
	int i = 0;
	do {
		ExtPoint* curPoint = [[thePoints objectAtIndex:i] retain];
		if ([newLn slopeUndefined]) {
			x3 = l.point.x;
			y3 = curPoint.point.y;
		} else {
			if (r.point.y == l.point.y) {
				x3 = curPoint.point.x;
				y3 = l.point.y;
			} else {
				x3 = (((curPoint.point.x + newLn.slope * (newLn.slope * l.point.x - l.point.y + curPoint.point.y)) / (1 + newLn.slope * newLn.slope)));
				y3 = ((newLn.slope * (x3 - l.point.x) + l.point.y));
			}
		}
		
		float x1 = curPoint.point.x;
		float y1 = curPoint.point.y;
		distance = (float) sqrt(pow((y1-y3), 2) + pow((x1-x3), 2));
		
		if (distance > maxDist) {
			maxDist = distance;
			farPt = i;
		}
		
		[curPoint release];
		i++;
	} while (i < [thePoints count] - 2);
	
	[newLn release];
	
	return farPt;	
}

- (void) dealloc {
	[points release];
	//[tempLns release];
	[tempHull release];
	[hull release];
	[hullIndices release];
	
	[super dealloc];
}

@end
