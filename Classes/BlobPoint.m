//
//  BlobPoint.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "BlobPoint.h"
#import "Blob.h";
#import "VectorMath.h"
#import "BlobbyViewController.h"

@implementation BlobPoint

@synthesize point;
@synthesize connectRadius;
@synthesize blob;
@synthesize velocity;
@synthesize acceleration;
@synthesize positionDeltas;
@synthesize beingDragged;
@synthesize blobRadius;
@synthesize redColour;
@synthesize greenColour;
@synthesize blueColour;

- (id) initWithPoint:(CGPoint) p {
	self = [super init];
	if (self) {
		self.point = p;
		connectRadius = 75;
		blobRadius = 22.0f;
		
		redColour = random() % 255;
		greenColour = random() % 255;
		blueColour = random() % 255;
		
		positionDeltas = [[NSMutableArray alloc] initWithCapacity:10];
	}
	
	return self;
}

- (void) calculateAcceleration {
	if (!(velocity.x == 0 && velocity.y == 0)) {
		float xExtraDecel = 0;
		float yExtraDecel = 0;
		
		//check if the blob needs extra deceleration because it is joined
		if ([blob blobPoints].count > 1) {
			xExtraDecel = EXTRA_BLOB_DECELERATION * [blob blobPoints].count;
			yExtraDecel = EXTRA_BLOB_DECELERATION * [blob blobPoints].count;
		}
		
		
		//set default deceleration values
		acceleration = CGPointMake(DEFAULT_DECELERATION + xExtraDecel, DEFAULT_DECELERATION + yExtraDecel);	
		
		//set the acceleration in accordance to the ratios of the velocity
		float x = velocity.x;
		float y = velocity.y;
		
		if (x < 0) {
			x *= -1;
		}
		if (y < 0) {
			y *= -1;
		}

		if (y != 0) {
			float xyRatio = x/y;
			acceleration.x *= xyRatio;	
		} else {
			float yxRatio = y/x;
			acceleration.y *= yxRatio;
		}

		//switch the acceleration depending on the direction of the velocity
		if (velocity.x < 0) {
			acceleration.x *= -1;
		}
		if (velocity.y < 0) {
			acceleration.y *= -1;
		}
	} else {
		acceleration = CGPointZero;
	}
}

- (void) calculateVelocity {
	if ([positionDeltas count] > 0) {
		
		//if the blob point is released next to an edge, reduce the velocity to a tenth
		BOOL blobPointOnEdge = NO;
		float minX = [BlobbyViewController minWidth] + blobRadius;
		float maxX = [BlobbyViewController maxWidth] - blobRadius;
		float minY = [BlobbyViewController minHeight] + blobRadius;
		float maxY = [BlobbyViewController maxHeight] - blobRadius;
		
		if (point.y == maxY) {
			velocity.y = EDGE_VELOCITY;
			blobPointOnEdge = YES;
		} if (point.y == minY) {
			velocity.y = -EDGE_VELOCITY;
			blobPointOnEdge = YES;			
		} else if (point.x == minX) {
			velocity.x = EDGE_VELOCITY;
			blobPointOnEdge = YES;
		} else if (point.x == maxX) {
			velocity.x = -EDGE_VELOCITY;
			blobPointOnEdge = YES;
		}
		
		if (blobPointOnEdge) {
			return;
		}
			
		float totalX = 0;
		float totalY = 0;
		
		//get an average of all the points
		for (NSValue* value in positionDeltas) {
			CGPoint delta = [value CGPointValue];
			totalX += delta.x;
			totalY += delta.y;
		}
		
		velocity.x = totalX/[positionDeltas count];
		velocity.y = totalY/[positionDeltas count];
		
		//check the velocity isn't too high
		if (velocity.x > MAX_VELOCITY) {
			velocity.x = MAX_VELOCITY;
		} else if (velocity.x < -MAX_VELOCITY) {
			velocity.x = -MAX_VELOCITY;
		}
		
		if (velocity.y > MAX_VELOCITY) {
			velocity.y = MAX_VELOCITY;
		} else if (velocity.y < -MAX_VELOCITY) {
			velocity.y = MAX_VELOCITY;
		}
		
	} else {
		velocity = CGPointZero;
	}
}

- (void) startDrag {
	//set the velocity and acceleration to zero now that the user is in control of the blob point
	velocity = CGPointZero;
	acceleration = CGPointZero;
	//clear the previous positions of the blob point so that we can start fresh for a new drag
	[positionDeltas removeAllObjects];
	
	beingDragged = YES;
}

- (void) finishDrag {
	beingDragged = NO;
	
	//calculate the velocity based on previous movements of the blob point
	[self calculateVelocity];
	
	//calculate the acceleration
	[self calculateAcceleration];
}

- (BOOL) updatePosition {
	BOOL returnValue = NO;
	
	if (self.acceleration.x != 0 || self.acceleration.y != 0) {
		CGPoint prevVelocity = velocity;
		
		velocity.x += acceleration.x;
		velocity.y += acceleration.y;
		
		if ((velocity.x >= 0 && prevVelocity.x < 0) || (velocity.x < 0 && prevVelocity.x >= 0)) {
			//different directions
			acceleration.x = 0;
			velocity.x = 0;
		}
		
		if ((velocity.y >= 0 && prevVelocity.y < 0) || (velocity.y < 0 && prevVelocity.y >= 0)) {
			//different directions
			acceleration.y = 0;
			velocity.y = 0;
		}
	}

	float minX = [BlobbyViewController minWidth] + blobRadius;
	float maxX = [BlobbyViewController maxWidth] - blobRadius;
	float minY = [BlobbyViewController minHeight] + blobRadius;
	float maxY = [BlobbyViewController maxHeight] - blobRadius;
	
	//check if the ad has "bumped" the blob point
	if (point.y < minY) {
		if (beingDragged) {
			point.y = minY;
		} else {
			velocity.y = 3;
			[self calculateAcceleration];			
		}
	}
		
	//make sure we don't go flying off the screen		
	if (self.velocity.x != 0 || self.velocity.y != 0) {
		point.x = point.x + velocity.x;
		point.y += velocity.y;
				
		if (point.x < minX) {
			point.x = minX + (minX - point.x);
			//switch velocity
			velocity.x *= -1;
			//switch acceleration
			acceleration.x *= -1;
		} else if (point.x > maxX) {
			point.x = maxX - (point.x - maxX);
			//switch velocity
			velocity.x *= -1;
			acceleration.x *= -1;
		}
		
		if (point.y < minY) {
			point.y = minY + (minY - point.y);
			//switch velocity
			velocity.y *= -1;
			//switch acceleration
			acceleration.y *= -1;
		} else if (point.y > maxY) {
			point.y = maxY - (point.y - maxY);
			//switch velocity
			velocity.y *= -1;
			//switch acceleration
			acceleration.y *= -1;
		}
		
		returnValue = YES;
	}
	
	return returnValue;
}

- (void) trackWithCurPos:(CGPoint) curPoint previousPoint:(CGPoint) prevPoint {

	//make adjustments if the points have moved past the min or max positions
//	curPoint = [self adjustPoint:curPoint];
//	prevPoint = [self adjustPoint:prevPoint];

		
	CGPoint delta = CGPointMake(curPoint.x - prevPoint.x, curPoint.y - prevPoint.y);
	NSValue* value = [NSValue valueWithCGPoint:delta];
	
	//check that the direction of the drag hasn't changed
	if (positionDeltas.count > 0) {
		NSValue* lastValue = [positionDeltas lastObject];
		CGPoint lastDelta = [lastValue CGPointValue];
		if ((lastDelta.x > 0 && delta.x < 0) || (lastDelta.x < 0 && delta.x > 0) || (lastDelta.y > 0 && delta.y < 0) || (lastDelta.y < 0 && delta.y > 0)) {
			//remove previous delta data as we've changed directions
			[positionDeltas removeAllObjects];
		} 
	}
	
	if ([positionDeltas count] >= NUM_DELTAS) {
		//remove the first item so we can add this one on the end
		[positionDeltas removeObjectAtIndex:0];
	}
	
	[positionDeltas addObject:value];
}

- (NSString*) description {
	NSString* description = [NSString stringWithFormat:@"x: %f, y: %f", point.x, point.y];
	return description;
}

- (void) dealloc {
	[positionDeltas release];
	
	[super dealloc];
}

@end
