//
//  Blob.h
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BezierPoint;
@class Hull;
@class BlobPoint;

#define BLOB_RADIUS 25.0f

@interface Blob : NSObject {

	Hull* hull; //this is the outer shell of points
	
	NSMutableArray* bezierPoints; //based of the points in the hull, bezier points are created and held in this
	
	CGMutablePathRef path;
	
	NSMutableArray* blobPoints; //this holds all the blob points on the screen
	
	UIColor* colour;
	
}

@property (readonly) NSMutableArray* blobPoints;
@property (readonly) NSMutableArray* bezierPoints;
@property (readonly) Hull* hull;
@property (readonly) CGMutablePathRef path;
@property (retain) UIColor* colour;

//construct the hull and figure out bezier points from the hull etc
- (void) build;

//num of Blob Points in the Blob (not the hull)
- (int) numBlobPoints;

//add a BlobPoint to the Blob (SHOULD ONLY BE DONE BEFORE build() IS CALLED)
- (void) addBlobPoint:(BlobPoint*) bp;

//get rid of all the blob points because we've joined blobs
- (void) clearBlobPoints;

//figures where the Bezier Points are going to be based off the Hull
- (void) generateBezierPoints;

//create a BezierPoint based off the coordinates, angle and radius
- (BezierPoint*) createBezierPointWithX:(float) x andY:(float) y andAngle:(float) angle andRadius:(float) radius;

//generate the control points based off the bezier points
- (void) generateControlPoints;

//tell the path to build its points so it is ready for drawing
- (void) generatePath;

- (BOOL) alreadyHasBlobPoint:(BlobPoint*) blobPoint;

@end
