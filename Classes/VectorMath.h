//
//  VectorMath.h
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface VectorMath : NSObject {

}

+ (CGPoint) randomPointInRect:(CGRect) rect;
+ (float) distanceBetweenPoint:(CGPoint) p1 andPoint:(CGPoint) p2;
+ (CGPoint) normalizeVector:(CGPoint) p;
+ (CGPoint) scaleVector:(CGPoint) v withScale:(float) scale;
+ (CGPoint) addVector:(CGPoint) v1 withVector:(CGPoint) v2;
+ (CGPoint) convertPolarPointWithDistance:(float) distance andAngle:(float) angle;

@end
