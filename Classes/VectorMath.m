//
//  VectorMath.m
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "VectorMath.h"


@implementation VectorMath

+ (CGPoint) randomPointInRect:(CGRect) rect {
	CGPoint point = CGPointMake(rect.origin.x + random() % (int) (rect.size.width - rect.origin.x), 
								rect.origin.y + random() % (int) (rect.size.height - rect.origin.y));
	
	return point;
}

+ (float) distanceBetweenPoint:(CGPoint) p1 andPoint:(CGPoint) p2 {
	float x = p2.x - p1.x;
	float y = p2.y - p1.y;
	
	float distance = sqrt(x*x + y*y);
	return distance;	
}

+ (CGPoint) normalizeVector:(CGPoint) p {
	float length = sqrt((p.x * p.x) + (p.y * p.y));
	CGPoint point = CGPointMake(p.x/length, p.y/length);

	return point;
}

+ (CGPoint) scaleVector:(CGPoint) v withScale:(float) scale {
	CGPoint point = CGPointMake(scale*v.x, scale*v.y);

	return point;
}

+ (CGPoint) addVector:(CGPoint) v1 withVector:(CGPoint) v2 {
	CGPoint point = CGPointMake(v1.x + v2.x, v1.y + v2.y);
	
	return point;
}

+ (CGPoint) convertPolarPointWithDistance:(float) distance andAngle:(float) angle {
	float x = distance*cos(angle);
	float y = distance*sin(angle);

	CGPoint point = CGPointMake(x, y);

	return point;
}

@end
