//
//  ControlsViewController.swift
//  Blobby
//
//  Created by Donna McCulloch on 5/05/2016.
//
//

import UIKit

class ControlsViewController: UIViewController {


    // MARK: - Properties

    private lazy var imageView: UIImageView = {
        let image = UIImage(named: "blobhelp")
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        return imageView
    }()

    private lazy var tabBarImage = UIImage(named: "navbarhelp")


    // MARK: - Init

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        title = NSLocalizedString("Help", comment: "Title for help tab.")
        tabBarItem.image = tabBarImage
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not supported")
    }


    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blackColor()

        view.addSubview(imageView)
        let centerXConstraint = NSLayoutConstraint(item: imageView, attribute: .CenterX, relatedBy: .Equal, toItem: view, attribute: .CenterX, multiplier: 1, constant: 0)
        let centerYConstraint = NSLayoutConstraint(item: imageView, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: 0)
        let constraints = [centerXConstraint, centerYConstraint]
        view.addConstraints(constraints)
        NSLayoutConstraint.activateConstraints(constraints)
    }

}
