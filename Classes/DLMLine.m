//
//  DLMLine.m
//  Blobby
//
//  Created by Donna McCulloch on 20/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "DLMLine.h"
#import "BlobPoint.h"


@implementation DLMLine

@synthesize point1;
@synthesize point2;
@synthesize slopeUndefined;
@synthesize slope;

- (id) initWithBlobPoint1:(BlobPoint*) bp1 andBlobPoint2:(BlobPoint*) bp2 {
	self = [super init];
	if (self) {
		point1 = [bp1 retain];
		point2 = [bp2 retain];
		
		
		if ([bp1 point].x == [bp2 point].x) {
			slopeUndefined = true;
		} else {
			slopeUndefined = false;	
			if ([bp2 point].y == [bp1 point].y) {
				slope = 0;
			} else {
				slope = ([bp2 point].y - [bp1 point].y) / ([bp2 point].x - [bp1 point].x);
			}
		}
	}
	
	return self;
}

- (BOOL) onLeftOfCheckPoint:(BlobPoint*) chkpt {
	if (slopeUndefined) {
		if ([chkpt point].x < [point1 point].x) {
			return true;
		} else {
			if ([chkpt point].x == [point1 point].x) {
				if ((([chkpt point].y > [point1 point].y) && ([chkpt point].y < [point2 point].y)) || 
					(([chkpt point].y > [point2 point].y) && ([chkpt point].y < [point1 point].y))) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	} else {            
		/* multiply the result to avoid the rounding error */
		float x3 = (([chkpt point].x + slope * (slope * [point1 point].x - [point1 point].y + [chkpt point].y)) / (1 + slope * slope)) * 10000;
		float y3 = (slope * (x3 / 10000 - [point1 point].x) + [point1 point].y) * 10000;
		
		if (slope == 0) {
			if (([chkpt point].y * 10000) > y3) {
				return true; 
			} else {
				return false;
			}
		} else {
			if (slope > 0) {
				if (x3 > ([chkpt point].x * 10000)) {
					return true; 
				} else {
					return false; 
				}
			} else {
				if (([chkpt point].x * 10000) > x3) {
					return true; 
				} else {
					return false; 
				} 
			}
		}
	}
}

- (void) dealloc {
	[point1 release];
	[point2 release];
	
	[super dealloc];
}

@end
