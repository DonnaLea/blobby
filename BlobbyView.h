//
//  BlobbyView.h
//  Blobby
//
//  Created by Donna McCulloch on 19/05/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlobbyViewController.h"

@class Blob;
@class BezierPoint;

@interface BlobbyView : UIView {
	IBOutlet BlobbyViewController* blobbyViewController;
}

- (void) drawBlob:(Blob*) blob;
- (void) drawBlobHull:(Blob*) blob;
- (void) drawBezierPoints:(Blob*) blob;
- (void) drawControlPoints:(BezierPoint*) bezierPoint;
- (void) drawBlobPoints:(Blob*) blob;

@end
