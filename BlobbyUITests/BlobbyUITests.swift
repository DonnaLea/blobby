//
//  BlobbyUITests.swift
//  BlobbyUITests
//
//  Created by Donna McCulloch on 5/05/2016.
//
//

import XCTest

class BlobbyUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func launchAppWithSnapshotSetup(screenshotTestName: String? = nil) {
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        let app = XCUIApplication()
        if let screenshotTestName = screenshotTestName {
            app.launchArguments = [screenshotTestName+"Test"]
        }
        setupSnapshot(app)
        app.launch()
    }

    func testBlobby5Screenshot() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let screenshotName = "01Blobby5Screen"
        launchAppWithSnapshotSetup(screenshotName)
        snapshot(screenshotName)

    }

    func testBlobby4Screenshot() {
        let screenshotName = "02Blobby4Screen"
        launchAppWithSnapshotSetup(screenshotName)
        snapshot(screenshotName)
    }

    func testBlobby3Screenshot() {
        let screenshotName = "03Blobby3Screen"
        launchAppWithSnapshotSetup(screenshotName)
        snapshot(screenshotName)
    }

    func testBlobbyMultiScreenshot() {
        let screenshotName = "04BlobbyMultiScreen"
        launchAppWithSnapshotSetup(screenshotName)
        snapshot(screenshotName)
    }

    func testHelpScreentshot() {
        launchAppWithSnapshotSetup()

        let tabBarsQuery = XCUIApplication().tabBars
        tabBarsQuery.buttons["Help"].tap()

        sleep(1)
        snapshot("00HelpScreen")
    }
}
